# videoplayer

This is a Video Player Application which gets videos from local storage of phone and shows them. User can select and play the video. It has all the option like in a video player app.


## App UI

![Screenshot_2021-07-12-16-55-49-027_com.example.videoplayer](/uploads/10ab9c4fe8bee8234724a8e2ad1cc8cb/Screenshot_2021-07-12-16-55-49-027_com.example.videoplayer.jpg)


![Screenshot_2021-07-12-16-55-36-959_com.example.videoplayer](/uploads/d93040b81dc25257a1a6611107684fec/Screenshot_2021-07-12-16-55-36-959_com.example.videoplayer.jpg)

![Screenshot_2021-07-12-16-55-18-451_com.example.videoplayer](/uploads/a698fcf37abd0f36ebc131cbc8416af3/Screenshot_2021-07-12-16-55-18-451_com.example.videoplayer.jpg)
