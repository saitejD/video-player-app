import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:videoplayer/main.dart';

import 'PlayFromAsset.dart';

class PlayFromInternet extends StatefulWidget {
  @override
  _PlayFromInternetState createState() => _PlayFromInternetState();
}

class _PlayFromInternetState extends State<PlayFromInternet> {
  final textController = TextEditingController(text: urlLandscapeVideo);
  late VideoPlayerController controller;

  @override
  void initState() {
    controller = VideoPlayerController.network(textController.text)
      ..addListener(() => setState(() {}))
      ..setLooping(true)
      ..initialize().then((value) => controller.play());
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          VideoPlayerWidget(controller: controller),
          buildTextField(),
        ],
      ),
    );
  }

  Widget buildTextField() => Container(
        padding: EdgeInsets.all(32),
        child: Row(
          children: [
            Expanded(
              child: TextFieldWidget(
                controller: textController,
                hintText: 'Enter Video Url',
              ),
            ),
            const SizedBox(width: 12),
            FloatingActionButtonWidget(
              onPressed: () {
                if (textController.text.trim().isEmpty) return;
                controller = VideoPlayerController.network(textController.text)
                  ..addListener(() {
                    setState(() {});
                  })
                  ..setLooping(true)
                  ..initialize().then((value) {
                    controller.play();
                    setState(() {});
                  });
              },
            ),
          ],
        ),
      );
}

class FloatingActionButtonWidget extends StatelessWidget {
  final VoidCallback onPressed;

  const FloatingActionButtonWidget({
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) => FloatingActionButton(
        child: Icon(Icons.add, size: 32),
        onPressed: onPressed,
        backgroundColor: Colors.purple,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      );
}

class TextFieldWidget extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;

  const TextFieldWidget({
    required this.controller,
    required this.hintText,
  });

  @override
  Widget build(BuildContext context) => TextField(
        controller: controller,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: TextStyle(color: Colors.grey),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(16),
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(16),
            borderSide: BorderSide(color: Theme.of(context).primaryColor),
          ),
        ),
      );
}
