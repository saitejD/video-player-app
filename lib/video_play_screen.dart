import 'dart:io';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:videoplayer/video_items.dart';

class VideoPlayScreen extends StatefulWidget {
  final File videoFile;
  const VideoPlayScreen({required this.videoFile});

  @override
  _VideoPlayScreenState createState() => _VideoPlayScreenState();
}

class _VideoPlayScreenState extends State<VideoPlayScreen> {
  late VideoPlayerController controller;
  // final asset = "assets/video.mp4";
  @override
  void initState() {
    print(widget.videoFile);
    controller = VideoPlayerController.file(widget.videoFile)
      ..addListener(() => setState(() {}))
      ..setLooping(true)
      ..initialize().then((value) => controller.play());
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // final isMuted = controller.value.volume == 0;
    return Scaffold(
      backgroundColor: Colors.black,
      body: VideoPlayerWidget(controller: controller),
    );
  }
}

class VideoPlayerWidget extends StatelessWidget {
  final VideoPlayerController controller;
  const VideoPlayerWidget({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.value.isInitialized
        ? VideoItems(
            videoPlayerController: controller, looping: false, autoplay: true)
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  Widget videoCard(video) {
    return Stack(
      children: [
        video.controller != null
            ? GestureDetector(
                onTap: () {
                  if (video.controller!.value.isPlaying) {
                    video.controller?.pause();
                  } else {
                    video.controller?.play();
                  }
                },
                child: SizedBox.expand(
                    child: FittedBox(
                  fit: BoxFit.cover,
                  child: SizedBox(
                    width: video.controller?.value.size.width ?? 0,
                    height: video.controller?.value.size.height ?? 0,
                    child: VideoPlayer(video.controller!),
                  ),
                )),
              )
            : Container(
                color: Colors.black,
                child: Center(
                  child: Text("Loading"),
                ),
              ),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                // VideoDescription(video.user, video.videoTitle, video.songName),
                // ActionsToolbar(video.likes, video.comments,
                //     "https://www.andersonsobelcosmetic.com/wp-content/uploads/2018/09/chin-implant-vs-fillers-best-for-improving-profile-bellevue-washington-chin-surgery.jpg"),
              ],
            ),
            SizedBox(height: 20)
          ],
        ),
      ],
    );
  }

//   Widget buildVideo() => Stack(
//         children: [
//           buildVideoPlayer(),
//           Positioned.fill(child: BasicOverlayWidget(controller: controller))
//         ],
//       );

//   Widget buildVideoPlayer() => AspectRatio(
//         aspectRatio: controller.value.aspectRatio,
//         child: VideoPlayer(this.controller),
//       );
// }

// class BasicOverlayWidget extends StatefulWidget {
//   final VideoPlayerController controller;

//   const BasicOverlayWidget({Key? key, required this.controller})
//       : super(key: key);

//   @override
//   _BasicOverlayWidgetState createState() => _BasicOverlayWidgetState();
// }

// class _BasicOverlayWidgetState extends State<BasicOverlayWidget> {
//   bool isTapped = false;
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () => setState(() {
//         isTapped = !isTapped;
//       }),
//       child: Stack(
//         children: [
//           isTapped ? buildPlayPause() : Container(),
//           Positioned(
//             bottom: 0,
//             left: 0,
//             right: 0,
//             child: buildIndicator(),
//           )
//         ],
//       ),
//     );
//   }

//   Widget buildIndicator() =>
//       VideoProgressIndicator(widget.controller, allowScrubbing: true);

//   Widget buildPlay() => widget.controller.value.isPlaying
//       ? Container(
//           alignment: Alignment.center,
//           color: Colors.black26,
//           child: Icon(Icons.pause, color: Colors.white, size: 80),
//         )
//       : Container(
//           alignment: Alignment.center,
//           color: Colors.black26,
//           child: Icon(Icons.play_arrow, color: Colors.white, size: 80),
//         );

//   Widget buildPlayPause() => Container(
//         alignment: Alignment.center,
//         color: Colors.black26,
//         child: IconButton(
//             icon: Icon(
//                 widget.controller.value.isPlaying
//                     ? Icons.pause
//                     : Icons.play_arrow,
//                 color: Colors.white,
//                 size: 80),
//             onPressed: () => widget.controller.value.isPlaying
//                 ? widget.controller.pause()
//                 : widget.controller.play()),
//       );
}
