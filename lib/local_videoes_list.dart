import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:videoplayer/PlayFromAsset.dart';
import 'package:videoplayer/video_items.dart';
import 'package:videoplayer/video_play_screen.dart';

final Directory _videoesDir = new Directory(
  '/storage/emulated/0/video songs',
);
// '/storage/emulated/0/Android/data/com.eclixtech.doc_scanner/files/CroppedImages',

class LocalVideoesListPage extends StatefulWidget {
  @override
  _LocalVideoesListPageState createState() => _LocalVideoesListPageState();
}

class _LocalVideoesListPageState extends State<LocalVideoesListPage> {
  // late VideoPlayerController controller;
  var videoesList = _videoesDir
      .listSync()
      .map((item) => item.path)
      .where((item) => item.endsWith(".mp4"))
      .toList(growable: false);
  // @override
  // void dispose() {
  //   controller.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    print(videoesList);
    return ListView.builder(
        itemCount: videoesList.length > 0 ? videoesList.length : 0,
        itemBuilder: (context, index) {
          File file = new File(videoesList[index]);
          String name = file.path.split('/').last;
          return ListTile(
            title: Text(
              name,
              style: TextStyle(
                fontSize: 16,
                // fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () async {
              // controller = await VideoPlayerController.file(file)
              //   ..addListener(() => setState(() {}))
              //   ..setLooping(true)
              //   ..initialize().then((value) => controller.play());
              print("pressed");
              Navigator.of(context).push(CupertinoPageRoute(
                  builder: (context) => VideoPlayScreen(videoFile: file)));
              // VideoPlayerWidget(controller: controller);
              // VideoItems(
              //   videoPlayerController: VideoPlayerController.file(
              //     file,
              //   ),
              //   looping: true,
              //   autoplay: true,
              // );
            },
          );
        });
  }
}
