import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

import 'video_items.dart';

class VideoesListPage extends StatefulWidget {
  @override
  _VideoesListPageState createState() => _VideoesListPageState();
}

class _VideoesListPageState extends State<VideoesListPage> {
  @override
  Widget build(BuildContext context) {
    // return Container(
    //   child: VideoItems(
    //     videoPlayerController: VideoPlayerController.asset(
    //       'assets/video.mp4',
    //       videoPlayerOptions: VideoPlayerOptions(),
    //     ),
    //     looping: false,
    //     autoplay: false,
    //   ),
    // );
    return PageView(
      scrollDirection: Axis.vertical,      
      children: [
        VideoItems(
          videoPlayerController: VideoPlayerController.asset(
            'assets/video.mp4',
          ),
          looping: false,
          autoplay: false,
        ),
        VideoItems(
          videoPlayerController: VideoPlayerController.network(
            'https://assets.mixkit.co/videos/preview/mixkit-group-of-friends-partying-happily-4640-large.mp4',
          ),
          looping: true,
          autoplay: true,
        ),
        VideoItems(
          videoPlayerController: VideoPlayerController.network(
              'https://assets.mixkit.co/videos/preview/mixkit-a-girl-blowing-a-bubble-gum-at-an-amusement-park-1226-large.mp4'),
          looping: false,
          autoplay: true,
        ),
        VideoItems(
          videoPlayerController: VideoPlayerController.network(
            'https://youtube.com/watch?v=HSAa9yi0OMA',
          ),
          autoplay: true,
          looping: false,
        ),
        VideoItems(
          videoPlayerController: VideoPlayerController.network(
              "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4"),
          looping: true,
          autoplay: false,
        ),
      ],
    );
    // ListView(
    //   shrinkWrap: true,
    //   padding: EdgeInsets.all(8),
    //   children: <Widget>[
    //     VideoItems(
    //       videoPlayerController: VideoPlayerController.network(
    //         'https://assets.mixkit.co/videos/preview/mixkit-group-of-friends-partying-happily-4640-large.mp4',
    //       ),
    //       looping: true,
    //       autoplay: true,
    //     ),
    //     VideoItems(
    //       videoPlayerController: VideoPlayerController.network(
    //           'https://assets.mixkit.co/videos/preview/mixkit-a-girl-blowing-a-bubble-gum-at-an-amusement-park-1226-large.mp4'),
    //       looping: false,
    //       autoplay: true,
    //     ),
    //     VideoItems(
    //       videoPlayerController: VideoPlayerController.asset(
    //         'assets/video.mp4',
    //       ),
    //       looping: false,
    //       autoplay: false,
    //     ),
    //     VideoItems(
    //       videoPlayerController: VideoPlayerController.network(
    //         'https://youtube.com/watch?v=HSAa9yi0OMA',
    //       ),
    //       autoplay: true,
    //       looping: false,
    //     ),
    //     VideoItems(
    //       videoPlayerController: VideoPlayerController.network(
    //           "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4"),
    //       looping: true,
    //       autoplay: false,
    //     ),
    //   ],
    // );
  }
}
