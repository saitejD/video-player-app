import 'package:flutter/material.dart';
import 'package:videoplayer/videoes_list_page.dart';
import 'package:videoplayer/local_videoes_list.dart';

import 'PlayFromInternet.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // elevation: 20,
        // titleSpacing: 20,
        title: Text("Video Player"),
      ),
      body: LocalVideoesListPage(),
    );
    //  DefaultTabController(
    //   length: 3,
    //   child: Scaffold(
    //     backgroundColor: Colors.blueGrey,
    //     appBar: AppBar(
    //       elevation: 20,
    //       // titleSpacing: 20,
    //       title: Text("Video Player"),
    //       bottom: TabBar(
    //         indicatorColor: Colors.white,
    //         tabs: [
    //           Tab(icon: Icon(Icons.attach_file), text: 'Videoes'),
    //           Tab(icon: Icon(Icons.assessment_outlined), text: 'Assets'),
    //           Tab(icon: Icon(Icons.ondemand_video_outlined), text: 'Network'),
    //         ],
    //       ),
    //     ),
    //     body: TabBarView(
    //       children: [
    //         LocalVideoesListPage(),
    //         VideoesListPage(),
    //         PlayFromInternet(),
    //       ],
    //     ),
    //   ),
    // );
  }
}
