import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:videoplayer/PlayFromAsset.dart';

class PlayFromPhone extends StatefulWidget {
  @override
  _PlayFromPhoneState createState() => _PlayFromPhoneState();
}

class _PlayFromPhoneState extends State<PlayFromPhone> {
  late VideoPlayerController controller;
  final File file = File(
      '/data/user/0/com.example.videoplayer/cache/file_picker/bin/video.mp4');
  @override
  void initState() {
    controller = VideoPlayerController.file(file)
      ..addListener(() => setState(() {}))
      ..setLooping(true)
      ..initialize().then((value) => controller.play());
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          VideoPlayerWidget(controller: controller),
          buildAddButton(),
        ],
      ),
    );
  }

  Widget buildAddButton() => Padding(
        padding: const EdgeInsets.all(32),
        child: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () async {
            final file = await pickFile();
            if (file == null) return;
            controller = VideoPlayerController.file(file)
              ..addListener(() {
                setState(() {});
              })
              ..setLooping(true)
              ..initialize().then((value) {
                controller.play();
                setState(() {});
              });
          },
        ),
      );

  Future<File?> pickFile() async {
    final result = await FilePicker.platform.pickFiles(type: FileType.video);
    if (result == null) return null;
    return File(result.files.single.path ?? "");
  }
}
