import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';

class VideoItems extends StatefulWidget {
  final VideoPlayerController videoPlayerController;
  final bool looping;
  final bool autoplay;

  VideoItems({
    required this.videoPlayerController,
    required this.looping,
    required this.autoplay,
  });

  @override
  _VideoItemsState createState() => _VideoItemsState();
}

class _VideoItemsState extends State<VideoItems> {
  late ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
    _chewieController = ChewieController(
      // deviceOrientationsAfterFullScreen: [DeviceOrientation.portraitUp],
      // deviceOrientationsOnEnterFullScreen: [
      //   DeviceOrientation.landscapeLeft,
      //   DeviceOrientation.landscapeRight
      // ],      
      optionsBuilder: (context, defaultOptions) async {
        await showDialog<void>(
          context: context,
          builder: (ctx) {
            return AlertDialog(
              content: ListView.builder(
                itemCount: defaultOptions.length,
                itemBuilder: (_, i) => ActionChip(
                  label: Text(defaultOptions[i].title),
                  onPressed: () => defaultOptions[i].onTap!(),
                ),
              ),
            );
          },
        );
      },
      videoPlayerController: widget.videoPlayerController,
      // aspectRatio: 0.6,
      autoInitialize: true,
      fullScreenByDefault: true,
      autoPlay: widget.autoplay,
      looping: widget.looping,
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: TextStyle(color: Colors.white),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    _chewieController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("called here");
    return Chewie(
      controller: _chewieController,
    );
  }
}
